const express = require('express');
const router = express.Router();
const { listCoffeeController } = require('../controllers/coffee');


router
.get('/machines/', listCoffeeController("machines"))
.get('/pods/', listCoffeeController("pods"))


module.exports = router; 
